import React from "react";
import { createBrowserRouter } from "react-router-dom";
import SignInForm from './components/SignInForm/SignInForm.js';

const routes = createBrowserRouter([
    {
        path: "/user",
        element: <SignInForm />,
    },
    {
        path: "/admin",
        element: <SignInForm />,
    },
]);

export default routes;
